<footer class="content-info" role="contentinfo">
  <div class="container">
  	<div class="row">
  		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
  			<div class="widget newest">
  				<h3 class="title"><?php echo ot_get_option('widget_noutati_titlu') ?></h3>
  				<p class="content">
  					<?php echo ot_get_option('widget_noutati_text') ?>
  					<a href="<?php echo ot_get_option('widget_noutati_link') ?>" class="more">Detalii +</a>
  				</p>
  			</div>
  		</div>
  		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
  			<div class="widget about-us">
  				<h3 class="title">Despre noi</h3>
  				<ul>
  					<li>
  						<i class="fa fa-map-marker"></i>EXELA. Building your concept.<br /><?php echo ot_get_option('adresa_fizica'); ?>
  					</li>
  					<li>
  						<i class="fa fa-phone"></i><?php echo ot_get_option('numar_de_telefon'); ?>
  					</li>
  					<li>
  						<i class="fa fa-envelope"></i><?php echo ot_get_option('adresa_de_email'); ?>
  					</li>
  				</ul>
  			</div>
  		</div>
  		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
  			<div class="widget newsletter">
  				<h3 class="title">Social Media</h3>
  				<span>Aboneaza-te la newsletter</span>
				<form class="newsletter-form form-inline" action="<?php echo ot_get_option('newsletter_widget_link_actiune'); ?>">
				  <div class="input-group">
				    <input type="email" class="newsletter-field form-control" placeholder="Adresa de email" required>
				    <span class="input-group-btn">
				      <button type="submit" class="search-submit btn btn-default"><?php _e('OK', 'sudo'); ?></button>
				    </span>
				  </div>
				</form>
				<div class="fb-like" data-href="<?php echo ot_get_option('adresa_de_facebook'); ?>" data-width="145px" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
  			</div>
  		</div>
  		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
  			<div class="widget info">
  				<h3 class="title">Info util</h3>
          <?php
            if (has_nav_menu('footer_navigation')) :
              wp_nav_menu(array('theme_location' => 'footer_navigation', 'walker' => new Sudo_Nav_Walker()));
            endif;
          ?>
  			</div>
  		</div>
  	</div>
  </div>
</footer>
<div class="subfooter first">
	<div class="container">
		<span class="copyright">
			Copyright &copy; <?php echo date('Y'); ?> Exela Concept<br />WebDesign by Media Serv
		</span>
	</div>
</div>
<div class="subfooter second">
	<div class="container">
		<p>Imobiliare Sibiu, anunturi cu vanzari si inchirieri de apartamente 2 camere, apartamente 3 camere, apartamente 4 camere, garsoniere,case ,terenuri intravilane si extravilane; spatii comerciale, birouri si industriale; constructii noi si constructii vechi</p>
	</div>
</div>

