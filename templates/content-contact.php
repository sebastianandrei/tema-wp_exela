<section class="contact-page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class="contact-col">
					<span class="title">
						Formular de contact
					</span>
					<?php echo do_shortcode('[contact-form-7 id="228" title="Formular de contact" html_class="form-horizontal custom-form"]'); ?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="contact-col">
					<span class="title dates">
						Date contact
					</span>
					<div class="contact-dates">
						<span><?php echo ot_get_option('adresa_fizica') ?></span>
						<span><?php echo ot_get_option('adresa_de_email') ?></span>
						<span>Tel: <?php echo ot_get_option('numar_de_telefon') ?></span>
						<span>Fax: <?php echo ot_get_option('numar_de_fax') ?></span>
						<span class="small_logo"><img src="<?php echo ot_get_option('small_logo') ?>" alt="<?php bloginfo('name'); ?>"></span>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
				<div class="contact-col">
					<span class="title">
						Localizare pe harta
					</span>
					<div class="gmap">
						<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script><div style='overflow:hidden;height:260px;width:100%;'><div id='gmap_canvas' style='height:260px;width:100%;'></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div> <a href='http://maps-generator.com/ro'>maps-generator</a> <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=7123dd02fe095ad2df8ae43c8d0b14b9d981728b'></script><script type='text/javascript'>function init_map(){var myOptions = {zoom:16,center:new google.maps.LatLng(45.7848142455245,24.13430216805341),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(45.7848142455245,24.13430216805341)});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>