<div class="container">
	<section class="welcome">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="welcome_message">
					<h2>Bun venit !</h2>
					<p><?php echo ot_get_option('welcome_message'); ?></p>
				</div>
				<div class="separator"></div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="welcome_content">
					<span class="title"><?php echo ot_get_option('titlu_titlu_introductiv'); ?></span>
					<div class="content">
						<?php echo ot_get_option('homepage_text'); ?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="welcome_gallery">
					<span class="title">Galerie foto</span>
					<div class="owl-home-carousel">
						<?php

							if ( function_exists( 'ot_get_option' ) ) {

							$images = explode( ',', ot_get_option( 'homepage_up_gallery', '' ) );

								if ( ! empty( $images ) ) {

							foreach( $images as $id ) {

								if ( ! empty( $id ) ) {

							$full_img_src = wp_get_attachment_image_src( $id, '' );

								echo '<a class="project-gallery-item" href="'. $full_img_src[0] .'" data-lightbox="hp-gallery">';

								echo '<div class="item" style="background: url('. $full_img_src[0] .')">';

								echo '</div>';

								echo '</a>';

										}

									}

								}

							}
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="separator"></div>
	</section>
</div>
<section class="our_recommend">
	<div class="section_heading" style="background: url(<?php echo ot_get_option('imagine_heading_recomandari'); ?>) no-repeat 50% 50%;">
		<div class="section-content">
			<div class="container">
				<span class="title">Recomandarea noastra!</span>
				<p class="short_desc">
				<span class="project_title"><?php echo ot_get_option('titlu_proiect'); ?></span>
				<?php echo ot_get_option('scurta_descriere_proiect'); ?>
				</p>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="container">
		<div class="row">
			<div class="project_content">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="project_image">
						<img src="<?php echo ot_get_option('imagine_descriptiva_proiect') ?>" alt="">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="project_entry">
						<?php echo ot_get_option('prezentare_proiect') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="services">
	<div class="container">
		<h2>Servicii</h2>
		<?php echo ot_get_option('scurta_descriere_servicii'); ?>
		<div class="separator"></div>
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="service-box first">
					<div class="image"></div>
					<span class="title"><?php echo ot_get_option('box_nr_1_titlu') ?></span>
					<p><?php echo ot_get_option('box_nr_1_text') ?></p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="service-box second">
					<div class=" image"></div>
					<span class="title"><?php echo ot_get_option('box_nr_2_titlu') ?></span>
					<p><?php echo ot_get_option('box_nr_1_text') ?></p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="service-box third">
					<div class=" image"></div>
					<span class="title"><?php echo ot_get_option('box_nr_3_titlu') ?></span>
					<p><?php echo ot_get_option('box_nr_1_text') ?></p>
				</div>
			</div>
		</div>
		<div class="separator"></div>
	</div>
</section>
<section class="apartments">
	<div class="container">
	<h2>Apartamente</h2>
		<div class="row">
	<?php 
	// WP_Query arguments
	$args = array (
		'post_type'              => array( 'galerie_apartamente' ),
		'post_status'            => array( 'publish' ),
		'posts_per_page'         => '6',
	);

	// The Query
	$query = new WP_Query( $args );

	// The Loop
	if ( $query->have_posts() ) { while ( $query->have_posts() ) { $query->the_post();
		$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		$post_id = get_the_ID();
	 ?>
			<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
				<a class="open-gallery" href="<?php echo $feat_image ?>" title="<?php the_title(); ?>" data-lightbox="apartment-<?php echo $post_id; ?>">
					<div class="ap_box" style="background: url(<?php echo $feat_image; ?>) no-repeat; background-size: cover;">
						<span class="ribbon <?php the_field('status_apartament_galerie'); ?>">
							<?php $status_ap = get_field('status_apartament_galerie'); if($status_ap === 'sell'){ echo 'Vandut'; }
							elseif($status_ap === 'offer'){
								echo 'De vanzare';
							}?>
						</span>
						<span class="rooms_number">
							<?php echo the_field('numar_camere_galerie'); ?>
						</span>
						<div class="hidden-gallery">
							<?php $gallery = get_post_gallery_images( $post ); ?>
							<?php foreach ($gallery as $image) {
								echo ' <a href="'. $image .'" data-lightbox="apartment-'. $post_id .'"><img src="'. $image .'"></a> ';
							} ?>
						</div>
					</div>
				</a>
			</div>
	<?php } } else { } wp_reset_postdata(); ?>
		</div>
	</div>
</section>
<section class="testimonials">
	<h2>Servicii</h2>
	<?php echo ot_get_option('scurta_descriere_servicii'); ?>
	<div class="separator"></div>

	<div class="container">
		<div class="row">
		<?php 
			// WP_Query arguments
			$args = array (
				'post_type'              => array( 'testimoniale' ),
				'post_status'            => array( 'publish' ),
				'posts_per_page'         => '6',
			);

			// The Query
			$query = new WP_Query( $args );

			// The Loop
			if ( $query->have_posts() ) { while ( $query->have_posts() ) { $query->the_post();
				$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		?>
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<div class="testimonial">
				<span class="image" style="background: url(<?php echo $feat_image ?>) no-repeat 50% 50%;"></span>
				<span class="title">
					<?php the_title(); ?>
				</span>
				<p class="content">
					<?php the_field('text_testimonial'); ?>
					<small><?php the_field('autor_testimonial'); ?></small>
				</p>
			</div>
		</div>
		<?php } } else { } wp_reset_postdata(); ?>
		</div>
	</div>
</section>