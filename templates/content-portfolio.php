<div class="container">
	<div class="row">
		<?php
		// args
		$args = array(
			'post_type'		=> 'proiecte',
			'meta_key'		=> 'status_proiect',
			'meta_value'	=> 'fin'
		);


		// query
		$the_query = new WP_Query( $args );

		?>
	<?php if( $the_query->have_posts() ): ?>
	<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
	 <section class="project-box">
	 	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	 		<h2 class="project-title"><?php the_title(); ?></h2>
	 		<div class="project-header" style="background: url(<?php the_field('banner_proiect'); ?>) no-repeat 50% 50%;">
	 			<?php 
	 				$status_proiect = get_field('status_proiect');
	 				if($status_proiect === 'dev'){
	 					$status_proiect = 'In dezvoltare';
	 				}elseif($status_proiect === 'fin'){
	 					$status_proiect = 'In curand';
	 				}
	 			 ?>
	 			<div class="ribbon <?php if(get_field('status_proiect') === 'dev'){ echo 'dev'; }elseif(get_field('status_proiect') === 'fin'){ echo 'fin'; } ?>">
	 				<?php echo $status_proiect; ?>
	 			</div>
	 			<span class="location">
	 				<?php the_field('locatie_proiect'); ?>
	 			</span>
	 		</div>
	 		<div class="project-content">
	 			<div class="row">
	 				<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
	 					<div class="nrap">
	 						<span>
	 							<?php the_field('numar_de_apartamente'); ?>
	 							<small>Apartamente</small>
	 						</span>
	 					</div>
	 				</div>
	 				<div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
	 					<div class="content">
	 						<?php the_field('text_proiect'); ?>
	 					</div>
	 				</div>
	 			</div>
	 		</div>
	 		<div class="project-gallery">
	 			<h4>Galerie foto</h4>

	 			<div class="gallery-slick-projects">
				  <?php $gallery = get_post_gallery_images( $post ); ?>
					<?php foreach ($gallery as $image) { ?>
						<div <?php post_class(); ?>>
							<a class="project-gallery-item" href="<?php echo $image; ?>" data-lightbox="image-<?php the_ID(); ?>">
								<div class="item" style="background:url(<?php echo $image ?>) 50% 50% no-repeat; height: 170px;">
								</div>
							</a>
						</div>
					<?php } ?>
				</div>
	 		</div>
	 	</div>
	 </section>
	 <div class="separator"></div>
	<?php endwhile; endif; ?>
	</div>
</div>