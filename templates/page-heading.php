<div class="page-heading" style="background: url(<?php if(is_post_type_archive('proiecte')){ echo ot_get_option('imagine_heading_proiecte'); }elseif(is_post_type_archive('apartamente')){ echo ot_get_option('imagine_heading_apartamente'); }{ the_field('imagine_heading'); } ?>) no-repeat 50% 50%;">
	<div class="page-title">
		<h1><?php sudo_title(); ?></h1>
		<?php if(get_field('subtitlu_pagina')){ ?>
		<span class="subtitle">
			<?php the_field('subtitlu_pagina'); ?>
		</span>
		<?php } ?>
	</div>
</div>