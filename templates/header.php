<header class="main-header">
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="logo">
                <a href="<?php bloginfo('url') ?>"><img src="<?php echo ot_get_option('logo_website') ?>" alt="<?php bloginfo('name') ?>"></a>
            </div>
        </div>
    </div>
    <div class="banner navbar navbar-default navbar-static-top" role="banner">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <nav class="collapse navbar-collapse" role="navigation">
          <?php
            if (has_nav_menu('primary_navigation')) :
              wp_nav_menu(array('theme_location' => 'primary_navigation', 'walker' => new Sudo_Nav_Walker(), 'menu_class' => 'nav navbar-nav'));
            endif;
          ?>
        </nav>
      </div>
    </div>
</header>
<?php if(is_front_page()){ ?>
    <section class="home-slider">
        <div class="owl-home-slider">
        <?php 
        // WP_Query arguments
            $args = array (
                'post_type'              => array( 'slider' ),
                'post_status'            => array( 'publish' ),
                'posts_per_page'         => '3',
            );

            // The Query
            $query = new WP_Query( $args );

            // The Loop
            if ( $query->have_posts() ) {  while ( $query->have_posts() ) { $query->the_post(); ?>
                <div class="item" style="background: url(<?php echo the_field('slider_image'); ?>); background-size: cover;">
                    <div class="slider-text">
                        <?php echo the_field('slide_text'); ?>
                    </div>
                </div>
            <?php    }  } else {  }  wp_reset_postdata(); ?>
        </div>
    </section>
<?php }elseif(is_page() || is_post_type_archive()){ ?>
    <?php get_template_part('templates/page-heading'); ?>
<?php }elseif(is_singular('apartamente')){ ?>
    <section class="apartments-browse">
        <div class="apartments-browse-cover" style="background: url(<?php the_field('imagine_cover_apartamente') ?>) no-repeat; background-size: cover; height: 480px;">
            <div class="cover-content">
                <?php the_field('text_slider_apartamente') ?>
            </div>
        </div>
    </section>
<?php } ?>