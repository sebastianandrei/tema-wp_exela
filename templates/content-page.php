<div class="container page">
	<div class="row">
		<?php the_content(); ?>
	</div>
</div>
<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>