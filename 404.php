<div class="alert alert-warning">
  <?php _e('Sorry, but the page you were trying to view does not exist.', 'sudo'); ?>
</div>

<p><?php _e('It looks like this was the result of either:', 'sudo'); ?></p>
<ul>
  <li><?php _e('a mistyped address', 'sudo'); ?></li>
  <li><?php _e('an out-of-date link', 'sudo'); ?></li>
</ul>

<?php get_search_form(); ?>