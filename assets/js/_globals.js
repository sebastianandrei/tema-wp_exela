// Our Javascript code

$(document).ready(function(){
	// Slick slider

  $('.gallery-slick').slick({
		infinite: true,
	  	slidesToShow: 6,
	  	slidesToScroll: 3,
	  	prevArrow:"<button type='button' data-role='none' class='slick-prev slick-arrow' role='button' style='display: block;'></button>",
      nextArrow:"<button type='button' data-role='none' class='slick-next slick-arrow' role='button' style='display: block;'></button>",
      adaptiveHeight: true
  });


  $('.owl-home-slider').owlCarousel({
      loop:true,
      margin:10,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true
          },
          600:{
              items:1,
              nav:false
          },
          1000:{
              items:1,
              nav:true,
              loop:false
          }
      }
  });

  $('.owl-home-carousel').owlCarousel({
      loop:true,
      margin:10,
      responsiveClass:true,
      nav: true,
      navText: [
        '<span class="prev"></span>',
        '<span class="next"></span>'
      ],
      responsive:{
          0:{
              items:1,
              nav:true
          },
          600:{
              items:1,
              nav:false
          },
          1000:{
              items:3,
              nav:true,
              loop:false
          }
      }
  });


  $('.gallery-slick-projects').owlCarousel({
      margin:10,
      responsiveClass:true,
      nav: true,
      navText: [
        '<span class="prev"></span>',
        '<span class="next"></span>'
      ],
      responsive:{
          0:{
              items:1,
              nav:true
          },
          600:{
              items:3,
              nav:false
          },
          1000:{
              items:6,
              nav:true,
              loop:false
          }
      }
  });


    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })

});