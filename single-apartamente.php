<section class="apartments-entry">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php while (have_posts()) : the_post(); ?>
				  <article <?php post_class(); ?>>
				    <div class="entry-content">
				      <?php the_content(); ?>
				    </div>
				  </article>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>