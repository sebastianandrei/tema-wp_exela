<?php
/*
Template Name: Apartamente
*/
?>
<div class="container">
	<div class="row">
		<section class="apartments">
			<?php 
				$terms = get_terms( 'ap_cat', array(
				    'orderby'    => 'count',
				    'hide_empty' => 0
				) );
			?>
			<?php
			// now run a query for each animal family
			foreach( $terms as $term ) {
			 
			    // Define the query
			    $args = array(
			        'post_type' => 'apartamente',
			        'ap_cat' => $term->slug
			    );
			    $query = new WP_Query( $args );
			?>
				 <section class="project-box">
				 	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				 		<h2 class="project-title"><?php echo $term->name ?></h2>
				 		<div class="project-header" style="background: url(<?php the_field('imagine_categorie', $term); ?>) no-repeat 50% 50%;">
				 			<?php 
				 				$status_proiect = get_field('stadiu_proiect_categorie', $term);
				 				if($status_proiect === 'in_dev'){
				 					$status_proiect = 'In dezvoltare';
				 				}elseif($status_proiect === 'final'){
				 					$status_proiect = 'Finalizat';
				 				}
				 			 ?>
				 			<div class="ribbon <?php if(get_field('stadiu_proiect_categorie', $term) === 'in_dev'){ echo 'dev'; }elseif(get_field('stadiu_proiect_categorie', $term) === 'final'){ echo 'fin'; } ?>">
				 				<?php echo $status_proiect; ?>
				 			</div>
				 			<span class="location">
				 				<?php the_field('text_imagine_categorie', $term); ?>
				 			</span>
				 		</div>
				 		<div class="project-content">
				 			<div class="row">
				 				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				 					<div class="content">
				 						<?php the_field('descriere_proiect', $term); ?>
				 					</div>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				 </section>

			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><div class="apartment_image" style="background: url(<?php echo $feat_image ?>) no-repeat 50% 50%;"></div></a>
					<a class="apartment_title" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</div>
			<?php endwhile; wp_reset_postdata(); } ?>
		</section>
	</div>
</div>