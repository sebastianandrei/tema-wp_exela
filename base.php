
<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
   <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
     
    <!-- Star Header -->
    <?php do_action('get_header'); get_template_part('templates/header'); ?>
    <!-- End Header -->

    <div class="wrap container-fluid" role="document">
    	<div class="content">
    		<main class="main" role="main">
          <?php if(!is_front_page()){ ?>
            <div class="container">
              <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
                            <?php if(function_exists('bcn_display'))
                            {
                                bcn_display();
                            }?>
                        </div>
                  </div>
              </div>
            </div>
          <?php } ?>
    			<?php include sudo_template_path(); ?>
    		</main><!-- End Main -->
      <?php if (sudo_display_sidebar()) : ?>
        <aside class="sidebar" role="complementary">
          <?php include sudo_sidebar_path(); ?>
        </aside><!-- /.sidebar -->
      <?php endif; ?>
      </div><!-- End Content / Row -->
    </div> <!-- End Wrap / Container -->
    <?php get_template_part('templates/footer');
      wp_footer();
     ?>
</body>
</html>
