<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  
  /* OptionTree is not loaded yet, or this is not an admin request */
  if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
    return false;
    
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'sidebar'       => ''
    ),
    'sections'        => array( 
      array(
        'id'          => 'prima_pagina',
        'title'       => __( 'Setari generale', 'exela' )
      ),
      array(
        'id'          => 'proiect_recomandat___prima_pagina',
        'title'       => __( 'Proiect recomandat - Prima pagina', 'exela' )
      ),
      array(
        'id'          => 'setari_generale',
        'title'       => __( 'Setari homepage', 'exela' )
      ),
      array(
        'id'          => 'servicii_homepage',
        'title'       => __( 'Servicii Homepage', 'exela' )
      ),
      array(
        'id'          => 'pagina_proiecte',
        'title'       => __( 'Pagina proiecte', 'exela' )
      ),
      array(
        'id'          => 'pagina_apartamente',
        'title'       => __( 'Pagina apartamente', 'exela' )
      ),
      array(
        'id'          => 'pagina_contact',
        'title'       => __( 'Pagina Contact', 'exela' )
      )
    ),
    'settings'        => array( 
      array(
        'id'          => 'logo_website',
        'label'       => __( 'Logo website', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'prima_pagina',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'imagine_heading_recomandari',
        'label'       => __( 'Imagine heading', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'proiect_recomandat___prima_pagina',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'proiecte_recomandate',
        'label'       => __( 'Proiecte recomandate', 'exela' ),
        'desc'        => __( 'Setari pentru sectiunea de proiecte recomandate de pe prima pagina. Introduceti informatiile care vor fi afisate folosind casutele de mai jos.', 'exela' ),
        'std'         => '',
        'type'        => 'textblock-titled',
        'section'     => 'proiect_recomandat___prima_pagina',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'titlu_proiect',
        'label'       => __( 'Titlu proiect', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'proiect_recomandat___prima_pagina',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'scurta_descriere_proiect',
        'label'       => __( 'Scurta descriere', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'proiect_recomandat___prima_pagina',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'prezentare_proiect',
        'label'       => __( 'Prezentare proiect', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'proiect_recomandat___prima_pagina',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'imagine_descriptiva_proiect',
        'label'       => __( 'Imagine descriptiva proiect', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'proiect_recomandat___prima_pagina',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'welcome_message',
        'label'       => __( 'Mesaj de bun venit', 'exela' ),
        'desc'        => __( 'Introduceti mesajul de bun venit care va afisat sub slider-ul de pe prima pagina', 'exela' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'setari_generale',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'titlu_titlu_introductiv',
        'label'       => __( 'Titlu "titlu introductiv"', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'setari_generale',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'homepage_text',
        'label'       => __( 'Titlu introductiv', 'exela' ),
        'desc'        => __( 'Introduceti text-ul care va afisat in categoria "Text introductiv".', 'exela' ),
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'setari_generale',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'homepage_up_gallery',
        'label'       => __( 'Galerie foto', 'exela' ),
        'desc'        => __( 'Selectati din galeria media sau din computer imaginile care doriti sa apara in galeria foto din partea dreapta a text-ului introductiv.', 'exela' ),
        'std'         => '',
        'type'        => 'gallery',
        'section'     => 'setari_generale',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'scurta_descriere_servicii',
        'label'       => __( 'Scurta descriere', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'servicii_homepage',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'box_nr_1_titlu',
        'label'       => __( 'Box nr. 1 titlu', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'servicii_homepage',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'box_nr_1_text',
        'label'       => __( 'Box nr.1 Text', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'servicii_homepage',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'box_nr_2_titlu',
        'label'       => __( 'Box nr.2 Titlu', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'servicii_homepage',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'box_nr_2_text',
        'label'       => __( 'Box nr.2 Text', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'servicii_homepage',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'box_nr_3_titlu',
        'label'       => __( 'Box nr.3 Titlu', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'servicii_homepage',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'box_nr_3_text',
        'label'       => __( 'Box  nr.3 Text', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'servicii_homepage',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'imagine_heading_proiecte',
        'label'       => __( 'Imagine heading', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'pagina_proiecte',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'imagine_heading_apartamente',
        'label'       => __( 'Imagine heading', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'pagina_apartamente',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'adresa_fizica',
        'label'       => __( 'Adresa fizica', 'exela' ),
        'desc'        => __( 'Ex: Sibiu, Strada Sperantei nr.2', 'exela' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'pagina_contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'adresa_de_email',
        'label'       => __( 'Adresa de email', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'pagina_contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'numar_de_telefon',
        'label'       => __( 'Numar de telefon', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'pagina_contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'numar_de_fax',
        'label'       => __( 'Numar de FAX', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'pagina_contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'small_logo',
        'label'       => __( 'Logo dimensiune mica', 'exela' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'pagina_contact',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      )
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;
  
}