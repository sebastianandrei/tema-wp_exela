<?php 

/**********************************************************
* Use this file to register custom post types
* 
*
* @param: https://codex.wordpress.org/Post_Types
**********************************************************/

// Custom post slide
function post_type_slider() {

	$labels = array(
		'name'                  => _x( 'Slidere', 'Post Type General Name', 'exela' ),
		'singular_name'         => _x( 'Slider', 'Post Type Singular Name', 'exela' ),
		'menu_name'             => __( 'Slidere', 'exela' ),
		'name_admin_bar'        => __( 'Slidere', 'exela' ),
		'parent_item_colon'     => __( 'Slide parinte', 'exela' ),
		'all_items'             => __( 'Toate slideurile', 'exela' ),
		'add_new_item'          => __( 'Adauga slide nou', 'exela' ),
		'add_new'               => __( 'Adauga slide', 'exela' ),
		'new_item'              => __( 'Slide nou', 'exela' ),
		'edit_item'             => __( 'Editeaza slide', 'exela' ),
		'update_item'           => __( 'Actualizeaza slide', 'exela' ),
		'view_item'             => __( 'Vezi slide', 'exela' ),
		'search_items'          => __( 'Cauta slide', 'exela' ),
		'not_found'             => __( 'Nu s-a gasit niciun slide', 'exela' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'exela' ),
		'items_list'            => __( 'Lista slide-uri', 'exela' ),
		'items_list_navigation' => __( 'Navigare lista slidere', 'exela' ),
		'filter_items_list'     => __( 'Filtreaza lista de slidere', 'exela' ),
	);
	$args = array(
		'label'                 => __( 'Slider', 'exela' ),
		'description'           => __( 'Slider Homepage', 'exela' ),
		'labels'                => $labels,
		'supports'              => array( 'title', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-slides',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'slider', $args );

}
add_action( 'init', 'post_type_slider', 0 );

// Register Custom Post Type
function post_type_galerie_apartamente() {

	$labels = array(
		'name'                  => _x( 'Galerie Apartamente', 'Post Type General Name', 'exela' ),
		'singular_name'         => _x( 'Galerie Apartamente', 'Post Type Singular Name', 'exela' ),
		'menu_name'             => __( 'Galerie apartamente', 'exela' ),
		'name_admin_bar'        => __( 'Galerie apartamente', 'exela' ),
		'parent_item_colon'     => __( 'Galerie parinte', 'exela' ),
		'all_items'             => __( 'Toate galeriile', 'exela' ),
		'add_new_item'          => __( 'Adauga galerie noua', 'exela' ),
		'add_new'               => __( 'Adauga galerie', 'exela' ),
		'new_item'              => __( 'Galerie noua', 'exela' ),
		'edit_item'             => __( 'Editeaza galerie', 'exela' ),
		'update_item'           => __( 'Actualizeaza galerie', 'exela' ),
		'view_item'             => __( 'Vezi galerie', 'exela' ),
		'search_items'          => __( 'Cauta galerie', 'exela' ),
		'not_found'             => __( 'Nu s-a gasit nici o galerie', 'exela' ),
		'not_found_in_trash'    => __( 'Nu s-a gasit nici o galerie foto in cosul de gunoi', 'exela' ),
		'items_list'            => __( 'Lista galerii foto', 'exela' ),
		'items_list_navigation' => __( 'Navigare lista de galerii foto', 'exela' ),
		'filter_items_list'     => __( 'Filtreaza lista de galerii foto', 'exela' ),
	);
	$args = array(
		'label'                 => __( 'Galerie Apartamente', 'exela' ),
		'description'           => __( 'Galerie foto pentru apartamente', 'exela' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'revisions', 'editor'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-gallery',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'galerie_apartamente', $args );

}
add_action( 'init', 'post_type_galerie_apartamente', 0 );

// Register Custom Post Type
function post_type_testimoniale() {

	$labels = array(
		'name'                  => _x( 'Testimoniale', 'Post Type General Name', 'exela' ),
		'singular_name'         => _x( 'Testimoniale', 'Post Type Singular Name', 'exela' ),
		'menu_name'             => __( 'Testimoniale', 'exela' ),
		'name_admin_bar'        => __( 'Testimoniale', 'exela' ),
		'parent_item_colon'     => __( 'Testimonial parinte:', 'exela' ),
		'all_items'             => __( 'Toate testimonialele', 'exela' ),
		'add_new_item'          => __( 'Adauga testimonial nou', 'exela' ),
		'add_new'               => __( 'Adauga testimonial', 'exela' ),
		'new_item'              => __( 'Testimonial nou', 'exela' ),
		'edit_item'             => __( 'Editeaza testimonial', 'exela' ),
		'update_item'           => __( 'Actualizeaza testimonial', 'exela' ),
		'view_item'             => __( 'Vezi testimonial', 'exela' ),
		'search_items'          => __( 'Cauta testimonial', 'exela' ),
		'not_found'             => __( 'Not found', 'exela' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'exela' ),
		'items_list'            => __( 'Lista testimoniale', 'exela' ),
		'items_list_navigation' => __( 'Navigare lista testimoniale', 'exela' ),
		'filter_items_list'     => __( 'FIltreaza lista de testimoniale', 'exela' ),
	);
	$args = array(
		'label'                 => __( 'Testimoniale', 'exela' ),
		'description'           => __( 'Adaugati testimoniale de la clienti', 'exela' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'revisions', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'testimoniale', $args );

}
add_action( 'init', 'post_type_testimoniale', 0 );

// Register Custom Post Type
function post_type_proiecte() {

	$labels = array(
		'name'                  => _x( 'Proiecte', 'Post Type General Name', 'exela' ),
		'singular_name'         => _x( 'Proiecte', 'Post Type Singular Name', 'exela' ),
		'menu_name'             => __( 'Proiecte', 'exela' ),
		'name_admin_bar'        => __( 'Proiecte', 'exela' ),
		'parent_item_colon'     => __( 'Proiect parinte', 'exela' ),
		'all_items'             => __( 'Toate proiectele', 'exela' ),
		'add_new_item'          => __( 'Adauga proiect nou', 'exela' ),
		'add_new'               => __( 'Adauga proiect', 'exela' ),
		'new_item'              => __( 'Proiect nou', 'exela' ),
		'edit_item'             => __( 'Editeaza proiect', 'exela' ),
		'update_item'           => __( 'Actualizeaza proiect', 'exela' ),
		'view_item'             => __( 'Vezi proiect', 'exela' ),
		'search_items'          => __( 'Cauta proiect', 'exela' ),
		'not_found'             => __( 'Not found', 'exela' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'exela' ),
		'items_list'            => __( 'Lista proiecte', 'exela' ),
		'items_list_navigation' => __( 'Navigare lista proiecte', 'exela' ),
		'filter_items_list'     => __( 'Filtreaza lista de proiecte', 'exela' ),
	);
	$args = array(
		'label'                 => __( 'Proiecte', 'exela' ),
		'description'           => __( 'Adaugati proiecte in aceasta lista', 'exela' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', ),
		'taxonomies'            => array( 'project_cat' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-building',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'proiecte', $args );

}
add_action( 'init', 'post_type_proiecte', 0 );

// Register Custom Post Type
function post_type_apartamente() {

	$labels = array(
		'name'                  => _x( 'Apartamente', 'Post Type General Name', 'exela' ),
		'singular_name'         => _x( 'Apartamente', 'Post Type Singular Name', 'exela' ),
		'menu_name'             => __( 'Proiecte apartamente', 'exela' ),
		'name_admin_bar'        => __( 'Proiecte apartamente', 'exela' ),
		'parent_item_colon'     => __( 'Proiect parinte', 'exela' ),
		'all_items'             => __( 'Toate apartamentele', 'exela' ),
		'add_new_item'          => __( 'Adauga apartament nou', 'exela' ),
		'add_new'               => __( 'Adauga apartament', 'exela' ),
		'new_item'              => __( 'Apartament nou', 'exela' ),
		'edit_item'             => __( 'Editeaza apartament', 'exela' ),
		'update_item'           => __( 'Actualizeaza apartament', 'exela' ),
		'view_item'             => __( 'Vezi apartament', 'exela' ),
		'search_items'          => __( 'Cauta apartament', 'exela' ),
		'not_found'             => __( 'Not found', 'exela' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'exela' ),
		'items_list'            => __( 'Lista apartamente', 'exela' ),
		'items_list_navigation' => __( 'Navigare lista apartament', 'exela' ),
		'filter_items_list'     => __( 'Filtreaza lista de apartamente', 'exela' ),
	);
	$args = array(
		'label'                 => __( 'Apartamente', 'exela' ),
		'description'           => __( 'Lista proiecte apartamente', 'exela' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', ),
		'taxonomies'            => array( 'ap_cat' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-home',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'apartamente', $args );

}
add_action( 'init', 'post_type_apartamente', 0 );