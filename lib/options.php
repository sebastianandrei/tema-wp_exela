<?php 
/* Verify WP_ENV status */

if (!defined('WP_ENV')) {
  define('WP_ENV', 'production');  // scripts.php checks for values 'production' or 'development'
}


/*
* Theme options. Some functions for theme support will be 
* here. Add your code bellow.
*/

/*****************************
* Theme Supports
* @ http://codex.wordpress.org/Function_Reference/add_theme_support
*****************************/

// Add support for title-tags
// http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
add_theme_support( 'title-tag' );

// Add support for HTML5 markup
// http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
add_theme_support('html5', array(
	'comment-list',
	'comment-form',
	'search-form',
	'gallery',
	'caption'	
	) );

// Add support for Post_Formats
// http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Formats
add_theme_support('post-formats', array(
	'aside',
	'gallery',
	'link',
	'image',
	'quote',
	'status',
	'video',
	'audio',
	'chat'
	));

// Add support for thumbnails
// http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
add_theme_support('post-thumbnails');

// Add support for Feeds
// http://codex.wordpress.org/Function_Reference/add_theme_support#Feed_Links
add_theme_support('automatic-feed-links');

// Additional support for some plugins
// Add support for Woocommerce. If you want to enable this feature, delete "//" before code.
// Attention ! Enable this feature after installing the WooCommerce plugin
// -----------------------------------------------------------------------------------------
// add_theme_support('woocommerce');

/**
 * Utility functions
 */
function is_element_empty($element) {
  $element = trim($element);
  return !empty($element);
}

// Tell WordPress to use searchform.php from the templates/ directory
function sudo_get_search_form() {
  $form = '';
  locate_template('/templates/searchform.php', true, false);
  return $form;
}
add_filter('get_search_form', 'sudo_get_search_form');

/**
 * Add page slug to body_class() classes if it doesn't exist
 */
function sudo_body_class($classes) {
  // Add post/page slug
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }
  return $classes;
}
add_filter('body_class', 'sudo_body_class');

// Hide admin bar Wordpress
add_filter('show_admin_bar', '__return_false');

