<?php

/*************************************************************
* In this file will be enabled the stylesheets and js plugins 
*
*************************************************************/

function sudo_scripts(){
	if(WP_ENV === 'development'){
		$assets = array(
			'css'       => '/assets/css/main.css',
			'js'        => '/assets/js/scripts.js',
			'modernizr' => '/assets/vendor/modernizr/modernizr.js',
			'jquery'    => '//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js'
			);
	}else{
		$assets = array(
			'css'       => '/assets/css/main.min.css',
			'js'        => '/assets/js/scripts.min.js',
			'modernizr' => '/assets/js/vendor/modernizr.min.js',
			'jquery'    => '//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'
			);
	}

	// Enqueue CSS
	wp_enqueue_style('sudo_css', get_template_directory_uri() . $assets['css'], false, null);

    // Enable JavaScripts: Jquery , Modernizr and our JS Scripts
     if (!is_admin() ) {
    wp_deregister_script('jquery');
    wp_register_script('jquery', $assets['jquery'], array(), null, true);
    add_filter('script_loader_src', 'sudo_jquery_local_fallback', 10, 2);
    }

    wp_enqueue_script('modernizr', get_template_directory_uri() . $assets['modernizr'], array(), null, true);
    wp_enqueue_script('jquery');
    wp_enqueue_script('sudo_js', get_template_directory_uri() . $assets['js'], array(), null, true);


    // Enable comment-reply.js for comments
	if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

  }

    add_action('wp_enqueue_scripts', 'sudo_scripts', 100);

    //  http://wordpress.stackexchange.com/a/12450
    function sudo_jquery_local_fallback($src, $handle = null) {
     static $add_jquery_fallback = false;
    if ($add_jquery_fallback) {
       echo '<script>window.jQuery || document.write(\'<script src="' . get_template_directory_uri() . '/assets/vendor/jquery/dis/jquery.min.js"><\/script>\')</script>' . "\n";
       $add_jquery_fallback = false;
       }
    if ($handle === 'jquery') {
       $add_jquery_fallback = true;
       }
       return $src;
       }
    add_action('wp_head', 'sudo_jquery_local_fallback');