<?php

  register_nav_menus(array(
    'primary_navigation' => __('Primary Navigation', 'sudo')
  ));

  register_nav_menus(array(
    'footer_navigation' => __('Footer Navigation', 'sudo')
  ));

  /**
 * Register sidebars
 */
function sudo_widgets_init() {
  register_sidebar(array(
    'name'          => __('Primary', 'sudo'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ));

  register_sidebar(array(
    'name'          => __('Footer', 'sudo'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ));
}
add_action('widgets_init', 'sudo_widgets_init');


   function sudo_title() {
  if (is_home()) {
    if (get_option('page_for_posts', true)) {
      echo get_the_title(get_option('page_for_posts', true));
    } else {
      _e('Ultimele produse', 'sudo');
    }
  } elseif (is_archive()) {
    $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
    if ($term) {
      echo $term->name;
    } elseif (is_post_type_archive()) {
      echo get_queried_object()->labels->name;
    } elseif (is_day()) {
      printf(__('Daily Archives: %s', 'sudo'), get_the_date());
    } elseif (is_month()) {
      printf(__('Monthly Archives: %s', 'sudo'), get_the_date('F Y'));
    } elseif (is_year()) {
      printf(__('Yearly Archives: %s', 'sudo'), get_the_date('Y'));
    } elseif (is_author()) {
      $author = get_queried_object();
      printf(__('Author Archives: %s', 'sudo'), $author->display_name);
    } else {
      single_cat_title();
    }
  } elseif (is_search()) {
    printf(__('Rezultatele cautarii pentru: <em class="search-query">%s</em>', 'sudo'), get_search_query());
  } elseif (is_404()) {
    _e('Nu s-a gasit niciun produs', 'sudo');
  } else {
    the_title();
  }
}


function add_parent_url_menu_class( $classes = array(), $item = false ) {
  // Get current URL
  $current_url = current_url();

  // Get homepage URL
  $homepage_url = trailingslashit( get_bloginfo( 'url' ) );

  // Exclude 404 and homepage
  if( is_404() or $item->url == $homepage_url )
    return $classes;

  if ( get_post_type() == "apartamente" || "proiecte" )
  {
    unset($classes[array_search('current_page_parent',$classes)]);
    if ( isset($item->url) )
      if ( strstr( $current_url, $item->url) )
        $classes[] = 'current-menu-item';
  }

  return $classes;
}

function current_url() {
  // Protocol
  $url = ( 'on' == $_SERVER['HTTPS'] ) ? 'https://' : 'http://';
  $url .= $_SERVER['SERVER_NAME'];

  // Port
  $url .= ( '80' == $_SERVER['SERVER_PORT'] ) ? '' : ':' . $_SERVER['SERVER_PORT'];
  $url .= $_SERVER['REQUEST_URI'];

  return trailingslashit( $url );
}
add_filter( 'nav_menu_css_class', 'add_parent_url_menu_class', 10, 3 );