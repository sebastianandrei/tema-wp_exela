<?php 

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_apartamente',
		'title' => 'Apartamente',
		'fields' => array (
			array (
				'key' => 'field_565565873adb4',
				'label' => 'Text slider',
				'name' => 'text_slider_apartamente',
				'type' => 'wysiwyg',
				'required' => 1,
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_565565b43adb5',
				'label' => 'Imagine cover',
				'name' => 'imagine_cover_apartamente',
				'type' => 'image',
				'required' => 1,
				'save_format' => 'url',
				'preview_size' => 'full',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'apartamente',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_galerie-foto-apartamente',
		'title' => 'Galerie foto apartamente',
		'fields' => array (
			array (
				'key' => 'field_564ed37214198',
				'label' => 'Status apartament',
				'name' => 'status_apartament_galerie',
				'type' => 'select',
				'instructions' => 'Introduceti statusul apartamentului. Ex: De vanzare , Vandut.',
				'required' => 1,
				'choices' => array (
					'offer' => 'De vanzare',
					'sell' => 'Vandut',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_564ed3a114199',
				'label' => 'Numar camere ',
				'name' => 'numar_camere_galerie',
				'type' => 'text',
				'instructions' => 'Introduceti numarul de camere sau tip-ul de imobil. Ex: 2 camere , 3 camere , Garsoniera , Penthouse ',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'galerie_apartamente',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_pagini',
		'title' => 'Pagini',
		'fields' => array (
			array (
				'key' => 'field_56541735fc1ba',
				'label' => 'Subtitlu pagina',
				'name' => 'subtitlu_pagina',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_565426e053071',
				'label' => 'Imagine heading',
				'name' => 'imagine_heading',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'full',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_proiecte',
		'title' => 'Proiecte',
		'fields' => array (
			array (
				'key' => 'field_564ed82165b3e',
				'label' => 'Banner proiect',
				'name' => 'banner_proiect',
				'type' => 'image',
				'required' => 1,
				'save_format' => 'url',
				'preview_size' => 'full',
				'library' => 'all',
			),
			array (
				'key' => 'field_564ed85265b3f',
				'label' => 'Locatie proiect',
				'name' => 'locatie_proiect',
				'type' => 'text',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_564ed86165b40',
				'label' => 'Status proiect',
				'name' => 'status_proiect',
				'type' => 'select',
				'required' => 1,
				'choices' => array (
					'dev' => 'In dezvoltare',
					'fin' => 'Proiect finalizat',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_564ed8e265b41',
				'label' => 'Numar de apartamente',
				'name' => 'numar_de_apartamente',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => 'ex: 120',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_564ed96658296',
				'label' => 'Text proiect',
				'name' => 'text_proiect',
				'type' => 'wysiwyg',
				'required' => 1,
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'proiecte',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_proiecte-apartamente',
		'title' => 'Proiecte apartamente',
		'fields' => array (
			array (
				'key' => 'field_56504e744b53e',
				'label' => 'Imagine categorie',
				'name' => 'imagine_categorie',
				'type' => 'image',
				'required' => 1,
				'save_format' => 'url',
				'preview_size' => 'full',
				'library' => 'all',
			),
			array (
				'key' => 'field_56504ec14b53f',
				'label' => 'Text imagine',
				'name' => 'text_imagine_categorie',
				'type' => 'text',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_56504edb4b540',
				'label' => 'Stadiu proiect',
				'name' => 'stadiu_proiect_categorie',
				'type' => 'select',
				'choices' => array (
					'in_dev' => 'In dezvoltare',
					'final' => 'Proiect finalizat',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_56504f3e4b541',
				'label' => 'Descriere proiect',
				'name' => 'descriere_proiect',
				'type' => 'wysiwyg',
				'required' => 1,
				'default_value' => '',
				'toolbar' => 'basic',
				'media_upload' => 'no',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'ap_cat',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_slider',
		'title' => 'Slider',
		'fields' => array (
			array (
				'key' => 'field_564cb12305781',
				'label' => 'Imagine slider',
				'name' => 'slider_image',
				'type' => 'image',
				'instructions' => 'Selectati o imagine din galeria media sau din computer.',
				'required' => 1,
				'save_format' => 'url',
				'preview_size' => 'full',
				'library' => 'all',
			),
			array (
				'key' => 'field_564cb17dd6883',
				'label' => 'Text slider',
				'name' => 'slide_text',
				'type' => 'wysiwyg',
				'instructions' => 'Folositi "<strong> </strong>" sau selectati text-ul care vreti sa fie bold si apasati pe butonul "B".',
				'required' => 1,
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'slider',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_testimoniale',
		'title' => 'Testimoniale',
		'fields' => array (
			array (
				'key' => 'field_564ed580e8a59',
				'label' => 'Text testimonial',
				'name' => 'text_testimonial',
				'type' => 'textarea',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_564ed5ade8a5a',
				'label' => 'Autor testimonial',
				'name' => 'autor_testimonial',
				'type' => 'text',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'testimoniale',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
