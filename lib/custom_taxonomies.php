<?php 

/**********************************************************
* Use this file to register custom taxonomies for custom
* post types
*
* @param: https://codex.wordpress.org/Taxonomies
**********************************************************/

// Register Custom Taxonomy
function taxonomy_projects() {

	$labels = array(
		'name'                       => _x( 'Categorii proiecte', 'Taxonomy General Name', 'exela' ),
		'singular_name'              => _x( 'Categorii proiecte', 'Taxonomy Singular Name', 'exela' ),
		'menu_name'                  => __( 'Categorii proiecte', 'exela' ),
		'all_items'                  => __( 'Toate categoriile', 'exela' ),
		'parent_item'                => __( 'Categorie parinte:', 'exela' ),
		'parent_item_colon'          => __( 'Categorie parinte:', 'exela' ),
		'new_item_name'              => __( 'Adauga nume categorie', 'exela' ),
		'add_new_item'               => __( 'Adauga categorie noua', 'exela' ),
		'edit_item'                  => __( 'Editeaza categorie', 'exela' ),
		'update_item'                => __( 'Actualizeaza categorie', 'exela' ),
		'view_item'                  => __( 'Vezi categorie', 'exela' ),
		'separate_items_with_commas' => __( 'Separa categoriile cu virgula', 'exela' ),
		'add_or_remove_items'        => __( 'Adauga sau sterge categorii', 'exela' ),
		'choose_from_most_used'      => __( 'Alege din cele mai utilizate categorii', 'exela' ),
		'popular_items'              => __( 'Categorii populare', 'exela' ),
		'search_items'               => __( 'Cauta categorii', 'exela' ),
		'not_found'                  => __( 'Not Found', 'exela' ),
		'items_list'                 => __( 'Lista categorii', 'exela' ),
		'items_list_navigation'      => __( 'Navigare lista categorii', 'exela' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'project_cat', array( 'proiecte' ), $args );

}
add_action( 'init', 'taxonomy_projects', 0 );

// Register Custom Taxonomy
function taxonomy_apartamente() {

	$labels = array(
		'name'                       => _x( 'Categorii', 'Taxonomy General Name', 'exela' ),
		'singular_name'              => _x( 'Categorii', 'Taxonomy Singular Name', 'exela' ),
		'menu_name'                  => __( 'Categorii', 'exela' ),
		'all_items'                  => __( 'Toate categoriile', 'exela' ),
		'parent_item'                => __( 'Categorie parinte:', 'exela' ),
		'parent_item_colon'          => __( 'Categorie parinte:', 'exela' ),
		'new_item_name'              => __( 'Nume categorie noua', 'exela' ),
		'add_new_item'               => __( 'Adauga categorie noua', 'exela' ),
		'edit_item'                  => __( 'Editeaza categorie', 'exela' ),
		'update_item'                => __( 'Actualizeaza categorie', 'exela' ),
		'view_item'                  => __( 'Vezi categorie', 'exela' ),
		'separate_items_with_commas' => __( 'Separa categoriile cu virgula', 'exela' ),
		'add_or_remove_items'        => __( 'Adauga sau sterge categorii', 'exela' ),
		'choose_from_most_used'      => __( 'Alege din cele mai utilizate', 'exela' ),
		'popular_items'              => __( 'Categorii populare', 'exela' ),
		'search_items'               => __( 'Cauta categorii', 'exela' ),
		'not_found'                  => __( 'Not Found', 'exela' ),
		'items_list'                 => __( 'Lista categorii', 'exela' ),
		'items_list_navigation'      => __( 'Navigare lista categorii', 'exela' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'ap_cat', array( 'apartamente' ), $args );

}
add_action( 'init', 'taxonomy_apartamente', 0 );