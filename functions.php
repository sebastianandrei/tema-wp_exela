<?php
/**
 * Sudo includes
 *
 * The $sudo_includes array determines the code library included in your theme.
 */
$sudo_includes = array(

  'lib/wrapper.php',                // Theme Wrapper
  'lib/options.php',                // Theme Options
  'lib/includes.php',               // Theme includes settings page.
  'lib/scripts.php',                // Stylesheets and JavaScript
  'lib/navigations.php',            // NavWalker for navigation menu
  'lib/utilis.php',                 // Use to register menus and sidebars
  'lib/sidebar.php',                // Sidebar Config
  'lib/post_types.php',             // Use this to register custom post types
  'lib/custom_taxonomies.php',      // Use this to register custom taxonomies
  'lib/acf_fields.php',             // Advances cusotm fields settings
);

// Hide Advanced custom fields menu for users

function remove_acf_menu() {
    remove_menu_page('edit.php?post_type=acf');
  }
  add_action( 'admin_menu', 'remove_acf_menu', 999);

require_once ('lib/media.php');

// Include OptionTree generated options
require( trailingslashit( get_template_directory() ) . 'admin/theme-options.php' );
// Hide for users 
add_filter( 'ot_show_pages', '__return_false' );

foreach ($sudo_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sudo'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);