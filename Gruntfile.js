'use strict';
module.exports = function(grunt){
	// Incarca toate task-urile
	require('load-grunt-tasks')(grunt);
	// Afiseaza timpul de executie cand rulezi o comanda in consola
	require('time-grunt')(grunt);

  var jsFileList = [
    'assets/vendor/bootstrap/js/transition.js',
    'assets/vendor/bootstrap/js/alert.js',
    'assets/vendor/bootstrap/js/button.js',
    'assets/vendor/bootstrap/js/carousel.js',
    'assets/vendor/bootstrap/js/collapse.js',
    'assets/vendor/bootstrap/js/dropdown.js',
    'assets/vendor/bootstrap/js/modal.js',
    'assets/vendor/bootstrap/js/tooltip.js',
    'assets/vendor/bootstrap/js/popover.js',
    'assets/vendor/bootstrap/js/scrollspy.js',
    'assets/vendor/bootstrap/js/tab.js',
    'assets/vendor/bootstrap/js/affix.js',
    'assets/js/plugins/*.js', 
    'assets/js/_*.js'
  ];

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'), // Citeste fisierul package.json

		// Task-uri
		// 1. Compilarea fisierelor LESS in Css
		less: {
			development: {
				options: {
					paths: ["asets/css"]
				},
				files: {
					"assets/css/main.css": "assets/less/_global.less"
				},
			},
		},

		// 2. Grunt verifica daca s-au facut modificari si executa comenzi
		//    pentru modificarea fisierelor de baza.

		watch: {
			// Verifica fisierele de stil
			styles: {
				files: [
				'assets/less/_global.less',
				'assets/less/_variables.less',
				'assets/less/_bootstrap.less',
				'assets/less/_responsive.less'
				],
				tasks:['less:development'],
				options: {
					nospawn: true
				},
			},

			// Verifica scripturile JS

			scripts: {
				files:[
				'Gruntfile.js',
			    'assets/js/*.js',
			    'assets/js/plugins/*.js',
			    '!assets/js/scripts.js',
                '!assets/**/*.min.*'			  
				],
				tasks: ['jshint', 'concat'],
				options: {
					spawn: false,
				},
			},
		},

		// 3. Verifica fisierele JS pentru greseli sau erori de cod si le valideaza

		jshint: {

			all: [
			'Gruntfile.js',
			'assets/js/*.js',
			'!assets/js/scripts.js',
			'!assets/**/*.js'
			],

			options: {

				asi:true,
				curly: false,
				eqeqeq: false,
				eqnull: true,
				browser: true,
				expr: true,
				laxbreak: true,
				latedef: true,
				newcap: true,
				node: true,
				noarg: true,
				strict: true,
				trailing: true,
				esnext: true,
				bitwise: false,
				undef: true,
				validthis: true,
				forin: true,
				globals: {
					jQuery: true,
					console: true,
					module: true,
				},
			},

	},

		// 4. Concatenare fisiere. Compileaza fiecare fisier in parte.

		concat: {
			options: {
				separator: ';',
			},

			dist: {
				src: [jsFileList],
				dest: 'assets/js/scripts.js',
			},
		},

		// 5. Minimizeaza fisierele cu pluginul ulgify.

		uglify: {
			options: {
				mangle: false
			},

			js_scripts: {
				files: {
					'assets/js/scripts.min.js': [jsFileList]
				}
			}
		},

		// 6. Minimizeaza fisierle CSS

		cssmin: {
			css:{
				src: 'assets/css/main.css',
				dest: 'assets/css/main.min.css'
			}
		}

	}); // Sfarsit grunt.initConfig

    // Inregistreaza comenzi pentru consola Grunt

    grunt.registerTask('default',[
    	'dev'
    	]);

    grunt.registerTask('dev', [
    	'jshint',
    	'less:development',
    	'concat'
    	]);

    grunt.registerTask('build', [
    	'jshint',
    	'uglify:js_scripts',
    	'cssmin:css',
    	]);


}; // Sfarsit function(grunt).